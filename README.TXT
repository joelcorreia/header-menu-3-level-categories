Header menu add level 3 sub categories for OpenCart 1.5.X
================================================
About
=======
This extension add to OpenCart header navigation three extra level for the category list.

Install
=======
1) copy catalog folders in the root folder of your website(opencart instalation folder)

This version overights the 2 original opencart files(header.php and header.tpl) and add an image file for the menu arrow,
if you have already modify the original files be aware that this will undo your modifications.
Files:
catalog->controller->common->header.php
catalog->view->theme->default->image->arrow-right.png
catalog->view->theme->template->common->header.tpl

It works great with an new open cart instalations, has supports for IE7 an IE8!

Beware of others modules that change the header behavior.

In custom themes you have to replace the code, of the header.php and  header.tpl, the changes are documented.


Change Log
========
Version 1.5.4.1
* some improvements on the hide of the 3rd level

Version 1.5.3.1
* initial release

Credits
========
#  Original developed to OpenCart 1.5.3.X by  Joel Correia: www.joelcorreia.pt And Il�dio Martins: http://my.opera.com/ilidiomartins/blog/